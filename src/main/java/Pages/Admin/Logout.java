package Pages.Admin;

import Pages.Base;
import com.aventstack.extentreports.Status;

import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.By;

import java.io.IOException;

public class Logout extends Base {

    public Logout(WebDriver driver) {
        super(driver);
    };

    By PROFILEDROPDOWN = By.xpath("//button[contains(.,'Hi, ')]");
    By LOGOUTBUTTON = By.xpath("//button[contains(.,'logout')]");

    // Method to click Profile
    public void click_profile_dropdown() {

        super.click(PROFILEDROPDOWN);
        //driver.findElement(PROFILEDROPDOWN).click();
    }

    // Method to click Logout
    public void click_logout() {

        super.click(LOGOUTBUTTON);
        //driver.findElement(LOGOUTBUTTON).click();
    }

}


