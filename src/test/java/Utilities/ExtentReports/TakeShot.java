package Utilities.ExtentReports;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class TakeShot {

    /**
     * Explanation needed here.
     * @param driver
     * @param screenShotName
     * @return
     */
    public static String capture(WebDriver driver,String screenShotName) //throws IOException
    {
        TakesScreenshot ts = (TakesScreenshot) driver;
        String dest = ts.getScreenshotAs(OutputType.BASE64);
        return "data:image/jpg;base64, " + dest ;
    }
}
