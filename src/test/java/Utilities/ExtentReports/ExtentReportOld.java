package Utilities.ExtentReports;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class ExtentReportOld
{
    protected ExtentHtmlReporter htmlReporter;
    protected ExtentReports extent;
    protected ExtentTest test;
    protected WebDriver driver;

    @BeforeSuite
    public void setUp() {
        htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") +"/test-output/HugoAutomation.html");

        // Set html reporter configuration
        htmlReporter.config().setReportName("Hugo Automation");
        htmlReporter.config().setDocumentTitle("Hugo Automation");

        // Hard coded configuration
        htmlReporter.config().setChartVisibilityOnOpen(true);
        htmlReporter.config().setTestViewChartLocation(ChartLocation.TOP);
        htmlReporter.config().setTheme(Theme.DARK);

        // Attach html reporter to extent report object
        extent = new ExtentReports();
        extent.attachReporter(htmlReporter);

        // Set extent system info
        extent.setSystemInfo("Environment", "QA");
        extent.setSystemInfo("User Name", System.getProperty("user.name"));
    }

    @AfterSuite
    public void tearDown() {
        extent.flush();
    }
}