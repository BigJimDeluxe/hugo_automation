package Tests;

import Utilities.ExtentReports.ExtentManager;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

public class BaseTest {
    public WebDriver driver;

    private static String getTestMethodName(ITestResult iTestResult) {
        return iTestResult.getMethod().getConstructorOrMethod().getName();
    }
    public WebDriver getDriver() {
        return driver;
    }
    private static ExtentReports extent = ExtentManager.getInstance();
    protected ExtentTest test;

    @BeforeClass
    public void setup (ITestContext context) {
        //Create a Chrome driver. All test classes use this.
     //   driver = new ChromeDriver();
        System.setProperty("webdriver.chrome.driver", "C:/Users/jjones1/Documents/chromedriver.exe");
        driver = new ChromeDriver();
        context.setAttribute("WebDriver", driver);

        //Maximize Window
        driver.manage().window().maximize();
    }

    @AfterClass
    public void teardown () {
        driver.quit();
    }
}