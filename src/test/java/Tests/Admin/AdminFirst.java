package Tests.Admin;

import Pages.Admin.Login;
import Pages.Admin.Logout;
import Tests.BaseTest;
import Utilities.ExtentReports.ExtentTestManager;
import org.testng.annotations.Test;

import java.lang.reflect.Method;

public class AdminFirst extends BaseTest {

    //  Login loginPage = new Login(driver);
    //  Logout logoutPage = new Logout(driver);

    @Test(priority = 0, description = "Verify the Admin Login Page")
    public void admin(Method method) throws InterruptedException {

        Login loginPage = new Login(driver);
        Logout logoutPage = new Logout(driver);

        ExtentTestManager.getTest().assignCategory("AdminFirst");

        ExtentTestManager.getTest().info("Verify the Admin Login Page");

        ExtentTestManager.getTest().info("Admin Page Launched");
        loginPage.AdminLogin();
        Thread.sleep(5);

        // Access Admin Page
        ExtentTestManager.getTest().info("Verify Admin Page is Ready");

        // Clear Existing User Name
        ExtentTestManager.getTest().info("Clear Existing User Name");
        loginPage.clear_username();

        // Enter User Name
        ExtentTestManager.getTest().info("Enter User Name");
        loginPage.enter_username();

        // Enter Password
        ExtentTestManager.getTest().info("Enter Password");
        loginPage.enter_password();

        // Click Login
        ExtentTestManager.getTest().info("Login to Admin");
        loginPage.click_login();
        Thread.sleep(5);

        // Click Profile
        ExtentTestManager.getTest().info("Click Profile");
        logoutPage.click_profile_dropdown();

        // Click Logout
        ExtentTestManager.getTest().info("Click Logout");
        logoutPage.click_logout();
    }

    @Test(priority = 0, description = "Verify the Admin Login Page Again")
    public void admin2(Method method) throws InterruptedException {
        Login loginPage = new Login(driver);
        Logout logoutPage = new Logout(driver);
        ExtentTestManager.getTest().assignCategory("AdminFirst");

        ExtentTestManager.getTest().info("Verify the Admin Login Page Again");


        ExtentTestManager.getTest().info("Admin Page Launched");
        loginPage.AdminLogin();
        Thread.sleep(5);

        // Access Admin Page
        ExtentTestManager.getTest().info("Verify Admin Page is Ready");
        loginPage.verify_admin_login_page();

        // Clear Existing User Name
        ExtentTestManager.getTest().info("Clear Existing User Name");
        loginPage.clear_username();

        // Enter User Name
        ExtentTestManager.getTest().info("Enter User Name");
        // loginPage.enter_username();

        // Enter Password
        ExtentTestManager.getTest().info("Enter Password");
        loginPage.enter_password();

        // Click Login
        ExtentTestManager.getTest().info("Login to Admin");
        loginPage.click_login();
        Thread.sleep(5);

        // Click Profile
        ExtentTestManager.getTest().info("Click Profile");
        logoutPage.click_profile_dropdown();

        // Click Logout
        ExtentTestManager.getTest().info("Click Logout");
        logoutPage.click_logout();

    }
}