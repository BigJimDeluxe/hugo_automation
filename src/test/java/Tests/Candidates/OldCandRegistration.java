package Tests.Candidates;

import Pages.Candidates.Marketing;
import Tests.BaseTest;
import Utilities.ExtentReports.ExtentTestManager;
import org.testng.annotations.Test;
import java.lang.reflect.Method;

public class OldCandRegistration extends BaseTest {

  //  Marketing marketingPage = new Marketing(driver);

    @Test(priority = 0, description="Verify the old candidate registration smoke test")
    public void old_cand_registration_smoke (Method method) throws InterruptedException {
        Marketing marketingPage = new Marketing(driver);

        ExtentTestManager.getTest().assignCategory("oldcandidateregistration");
        ExtentTestManager.getTest().info("Verify the new candidate registration smoke test");

        // launch Hugo
        ExtentTestManager.getTest().info("Hugo Launched");
        marketingPage.gotoHugo();

        // verify text
        ExtentTestManager.getTest().info("Join Text verified");
        marketingPage.verify_join_button_text("Joinn");

        // verify join button
        ExtentTestManager.getTest().info("Join Button verified");
        marketingPage.verify_join_button();

        // click join
        ExtentTestManager.getTest().info("Join Button clicked");
        marketingPage.click_join();

//Quit browser
    //    driver.quit();
    }

    @Test( priority = 0, dependsOnMethods = { "old_cand_registration_smoke" }, description="Verify the old candidate registration smoke test again")
    public void old_cand_registration_smoke_1 (Method method) throws InterruptedException {
        Marketing marketingPage = new Marketing(driver);

        ExtentTestManager.getTest().assignCategory("oldcandidateregistration");
        ExtentTestManager.getTest().info("Verify the new candidate registration smoke test");

        // launch Hugo
        ExtentTestManager.getTest().info("Hugo Launched");
        marketingPage.gotoHugo();

        // verify text
        ExtentTestManager.getTest().info("Join Text verified");
        marketingPage.verify_join_button_text("Join");

        // verify join button
        ExtentTestManager.getTest().info("Join Button verified");
        marketingPage.verify_join_button();

        // click join
        ExtentTestManager.getTest().info("Join Button clicked");
        marketingPage.click_join();

//Quit browser
        //    driver.quit();
    }

}
