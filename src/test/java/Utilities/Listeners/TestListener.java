package Utilities.Listeners;

import Tests.BaseTest;
import Utilities.ExtentReports.ExtentManager;
import Utilities.ExtentReports.ExtentTestManager;
import Utilities.ExtentReports.TakeShot;
import org.openqa.selenium.WebDriver;
import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import java.io.IOException;


public class TestListener extends BaseTest implements ITestListener, IInvokedMethodListener {
    private static ExtentReports extent = ExtentManager.getInstance();
    public ExtentHtmlReporter htmlReporter;
    //  public ExtentReports extent;
    public ExtentTest logger;
    //  public WebDriver driver;
    //WebDriver driver=null;
    String filePath = System.getProperty("user.dir");

    //  String filePath = "D:\\SCREENSHOTS";
    public WebDriver getDriver() {
        return driver;
    }

    @Override
    public void beforeInvocation(IInvokedMethod method, ITestResult testResult) {
        if (method.isTestMethod()) {
            //  WebDriver driver;

            // driver = new ChromeDriver();
            //  DriverContext.setDriver(driver);

        }
    }

    @Override
    public void afterInvocation(IInvokedMethod method, ITestResult testResult) {

    }

    @Override
    public void onStart(ITestContext context) {
        System.out.println("*** Test Suite " + context.getName() + " started ***");
    }

    @Override
    public void onFinish(ITestContext context) {
        System.out.println(("*** Test Suite " + context.getName() + " ending ***"));
        ExtentTestManager.endTest();
        ExtentManager.getInstance().flush();
    }

    @Override
    public void onTestStart(ITestResult result) {
        System.out.println(("*** Running test method " + result.getMethod().getMethodName() + "..."));
        ExtentTestManager.startTest(result.getMethod().getMethodName());
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        System.out.println("*** Executed " + result.getMethod().getMethodName() + " test successfully...");
        ExtentTestManager.getTest().log(Status.PASS, "Test passed");
    }

    @Override
    public void onTestFailure(ITestResult result) {

        ExtentTestManager.getTest().log(Status.FAIL, "Test failed");
        ITestContext context = result.getTestContext();
        driver = (WebDriver) context.getAttribute("WebDriver");

        String methodName = result.getMethod().getMethodName();

        try {

            //  ExtentTestManagerAvent.getTest().log(Status.FAIL, "Failed Case is: " + result.getName());
            ExtentTestManager.getTest().addScreenCaptureFromPath(TakeShot.capture(driver, result.getName()));
            //test.addScreenCaptureFromPath(screenShotPath);
            ExtentTestManager.getTest().log(Status.FAIL, result.getName() + " FAIL with error " + result.getThrowable());

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void onTestSkipped(ITestResult result) {
        test = extent.createTest(result.getName());
        test.skip(MarkupHelper.createLabel(result.getName() + " - Test Case Skipped", ExtentColor.YELLOW));
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
        System.out.println("*** Test failed but within percentage % " + result.getMethod().getMethodName());
    }

}