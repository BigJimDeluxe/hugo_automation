package Pages.Candidates;

import Pages.Base;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.By;

public class Marketing extends Base {

    public Marketing(WebDriver driver) {
        super(driver);
    };

    String baseURL = "https://hugo-public-qa.azurewebsites.net";

    By JOINBUTTON = By.xpath("//button[contains(.,'Join')]");

    // WebDriver driver = new ChromeDriver();
    // Method to access HUGO url
    public Marketing gotoHugo() {
        //  WebDriver driver = new ChromeDriver();
        driver.get(baseURL);
        return this;
    }

    // Method to click Join
    public void click_join() {
        super.click(JOINBUTTON);
        //  driver.findElement(JOINBUTTON).click();
    }

    // Method to Verify Join is ready
    public void verify_join_button() {
        super.waitVisibility(JOINBUTTON);
        //  wait.until(ExpectedConditions.visibilityOfElementLocated(JOINBUTTON));
    }

    // Method to Verify Join text is present
    public void verify_join_button_text(String jointext) {

        super.waitTextVisibility(JOINBUTTON, jointext);

    }

}
