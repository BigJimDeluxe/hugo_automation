package Pages.Candidates;

import Pages.Base;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class RegisterPositionInterests extends Base {

    public RegisterPositionInterests(WebDriver driver) {
        super(driver);
    };

    By JOINBUTTON = By.xpath("//button[contains(.,'Join')]");

    By PROJECT = By.xpath("//span[contains(.,'Project')][1]");
    By PROJECTPERM = By.xpath("//span[contains(.,'Project-to-Perm')][1]");
    By PERMANENT = By.xpath("//span[contains(.,'Permanent')][1]");
    By POSITIONINTERESTCHECKBOX1 = By.xpath("//span[contains(.,'Project')]/parent::label/child::div");
    By POSITIONINTERESTCHECKBOX2 = By.xpath("//span[contains(.,'Project-to-Perm')]/parent::label/child::div");
    By POSITIONINTERESTCHECKBOX3 = By.xpath("//span[contains(.,'Permanent')]/parent::label/child::div");
    By POSTITIONINTERESTCHECKBOXESCHECKED = By.xpath("//*[@preserveAspectRatio='xMidYMid meet']");
    By PROGRESSBAR = By.xpath("//*[@stroke='#F2F2F2']");
    By NEXTDISABLED2 = By.xpath("//button[@disabled]");
    By NEXTDISABLED3 = By.xpath("//button[@class='StyledButton-sc-323bzc-0 wUMvJ']");
    By VERIFYPOSITIONINTERESTPAGE = By.xpath("//p[contains(.,'What type of position are you interested in?')]");

    // Method to click Join
    public void click_next(){
        driver.findElement(JOINBUTTON).click();
    }

    // Method to Verify Join is ready
    public void verify_join_button(){
      //  WebDriverWait wait = new WebDriverWait(driver, 3);
        wait.until(ExpectedConditions.visibilityOfElementLocated(JOINBUTTON));
    }

    // Method to Verify Join text is present
    public void verify_join_button_text(String jointext){
        // WebDriverWait wait = new WebDriverWait(driver, 10);

      //  wait.until(ExpectedConditions.textToBePresentInElement((WebElement) JOINBUTTON, jointext));
        wait.until(ExpectedConditions.textToBePresentInElementLocated((By) JOINBUTTON,jointext));

        System.out.println();
    }

}
