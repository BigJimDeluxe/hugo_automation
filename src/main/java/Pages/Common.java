package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Common extends Base {

    public Common(WebDriver driver) {
        super(driver);
    };

    String adminURL =  "https://hugo-admin-qa.azurewebsites.net";

    By NEXTTEXT = By.xpath("(//span[contains(.,'NEXT')]/parent::div/child::div");
  //  NEXTTEXT2 = (By.XPATH, '//span[contains(.,"NEXT")]/parent::div/child::div')

    // Method to verify Next text is above Next icon
    public void verify_next_text()
    {  super.waitVisibility(NEXTTEXT);
      //  super.waitTextVisibility(VERIFYADMINLOGINPAGETEXT,"Welcome Back!");

    }


}
