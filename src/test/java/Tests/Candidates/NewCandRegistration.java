package Tests.Candidates;

import Pages.Candidates.Marketing;
import Pages.Common;
import Tests.BaseTest;
import Utilities.ExtentReports.ExtentTestManager;
import org.testng.annotations.Test;

import java.lang.reflect.Method;

public class NewCandRegistration extends BaseTest {
    //  Marketing marketingPage = new Marketing(driver);

    @Test(priority = 0, description = "Verify the new candidate registration smoke test")
    public void new_cand_registration_smoke(Method method) throws InterruptedException {
        Marketing marketingPage = new Marketing(driver);

        Common commonPage = new Common(driver);
        ExtentTestManager.getTest().assignCategory("newcandidateregistration");
        ExtentTestManager.getTest().info("Verify the new candidate registration smoke test");

        // launch Hugo
        ExtentTestManager.getTest().info("Hugo Launched");
        marketingPage.gotoHugo();

        // verify text
        ExtentTestManager.getTest().info("Join Text verified");
        marketingPage.verify_join_button_text("Join");

        // verify join button
        ExtentTestManager.getTest().info("Join Button verified");
        marketingPage.verify_join_button();

        // click join
        ExtentTestManager.getTest().info("Join Button clicked");
        marketingPage.click_join();

//Quit browser
        //    driver.quit();
    }

    @Test(priority = 0, description = "Verify the new candidate registration smoke test again")
    public void new_cand_registration_smoke_1(Method method) throws InterruptedException {
        Marketing marketingPage = new Marketing(driver);
        Common commonPage = new Common(driver);

        ExtentTestManager.getTest().assignCategory("newcandidateregistration");
        ExtentTestManager.getTest().info("Verify the new candidate registration smoke test");

        // launch Hugo
        ExtentTestManager.getTest().info("Hugo Launched");
        marketingPage.gotoHugo();

        // verify text
        ExtentTestManager.getTest().info("Join Text verified");
        marketingPage.verify_join_button_text("Join");

        // verify join button
        ExtentTestManager.getTest().info("Join Button verified");
        marketingPage.verify_join_button();

        // click join
        ExtentTestManager.getTest().info("Join Button clicked");
        marketingPage.click_join();


//Quit browser
        //    driver.quit();
    }

}
