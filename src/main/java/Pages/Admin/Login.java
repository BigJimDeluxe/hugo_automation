package Pages.Admin;

import Pages.Base;
import com.aventstack.extentreports.Status;

import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.By;

import java.io.IOException;

public class Login extends Base {

    public Login(WebDriver driver) {
        super(driver);
    };

    String adminURL = "https://hugo-admin-qa.azurewebsites.net";

    By VERIFYADMINLOGINPAGETEXT = By.xpath("(//div[contains(.,'Welcome Back!')])[last()]");
    By ADMINUSERNAME = By.name("email");
    By ADMINPASSWORD = By.name("password");
    // By LOGINBUTTON = By.xpath("//*[contains(span, 'Log In')]");
    By LOGINBUTTON = By.xpath("//span[contains(., 'Log In')]");

    // Method to access Admin url
    public Login AdminLogin() {
        //  WebDriver driver = new ChromeDriver();
        driver.get(adminURL);
        //  driver.navigate().refresh();
        return this;
    }

    // Method to verify page is ready
    public void verify_admin_login_page() {
        super.waitTextVisibility(VERIFYADMINLOGINPAGETEXT, "Welcome Back!");
    }


    // Method to clear existing text from username field
    public void clear_username() {
        super.clearText(ADMINUSERNAME);
    }

    // Method to enter username
    public void enter_username() {
        super.writeText(ADMINUSERNAME, "test.user.1@de.com");
        // wait.until()findElement(ADMINUSERNAME).sendKeys("test.user.1@de.com");
    }

    // Method to enter password
    public void enter_password() {
        super.writeText(ADMINPASSWORD, "d1G1t3!3");
        //  driver.findElement(ADMINPASSWORD).sendKeys("d1G1t3!3'");
    }

    // Method to click Login
    public void click_login() {
        super.click(LOGINBUTTON);

        //   driver.findElement(LOGINBUTTON).click();
    }


}
