package Pages.Admin;

import Pages.Base;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class Dashboard extends Base {

    public Dashboard(WebDriver driver) {
        super(driver);
    };

    By JOINBUTTON = By.xpath("//button[contains(.,'Join')]");

    // Method to click Join
    public void click_join(){
        driver.findElement(JOINBUTTON).click();
    }

    // Method to Verify Join is ready
    public void verify_join_button(){
      //  WebDriverWait wait = new WebDriverWait(driver, 3);
        wait.until(ExpectedConditions.visibilityOfElementLocated(JOINBUTTON));
    }

    // Method to Verify Join text is present
    public void verify_join_button_text(String jointext){
        // WebDriverWait wait = new WebDriverWait(driver, 10);

      //  wait.until(ExpectedConditions.textToBePresentInElement((WebElement) JOINBUTTON, jointext));
        wait.until(ExpectedConditions.textToBePresentInElementLocated((By) JOINBUTTON,jointext));

        System.out.println();
    }

    // Method to Verify Join text is present
    public void verify_join_button_text_try(String jointext){
   try {

       wait.until(ExpectedConditions.textToBePresentInElementLocated((By) JOINBUTTON, jointext));
   }
   catch(TimeoutException e)
   {
       System.out.println("notexist");
   }
    }

}
